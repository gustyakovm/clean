<?php 

	add_action('wp_ajax_load_more_content', 'load_more_content');
	add_action('wp_ajax_nopriv_load_more_content', 'load_more_content');
	
	function load_more_content()
	{
		$new_posts = "";
		$post_query_args = $_POST['next_params'];
		$i = 1;

		$mquery = new WP_Query($post_query_args);
		 while ($mquery->have_posts()) : $mquery->the_post();
		 	$new_posts .= my_load_template_part('blocks/post-template', array('i' => $i));
		 	$i++;
		 endwhile;

		 $next_params = null;
		 if($mquery->query['paged'] < $mquery->max_num_pages){
		    $next_params = os_get_next_posts_link($mquery);
		  }


		  $json_response = json_encode(array());
		  if($new_posts != ''){
		    $json_response = json_encode(array('success' => TRUE, 'has_posts' => TRUE, 'new_posts' => $new_posts, 'next_params' => $next_params, 'no_more_posts_message' => 'No more posts'));
		  }else{
		    $json_response = json_encode(array('success' => TRUE, 'has_posts' => FALSE, 'no_more_posts_message' => 'No more posts'));
		  }
		  echo $json_response;
		  wp_reset_postdata();
		  wp_die();
	}

	// do_action('wp_load_more_content');


	// Generate next page link for infinite scroll
	function os_get_next_posts_link($os_query){
	  $current_page = ( isset($os_query->query['paged']) ) ? $os_query->query['paged'] : 1;
	  $next_page = ($current_page < $os_query->max_num_pages) ? $current_page + 1 : false;
	  if($next_page){
	    return http_build_query(wp_parse_args( array('paged' => $next_page), $os_query->query));
	  }else{
	    return false;
	  }
	}

 ?>