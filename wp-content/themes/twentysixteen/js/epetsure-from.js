( function( $ ) {
	console.log('2222');
	$( function() {
		$('.show_get_policy_form').on('click', function(){ 
      alert("dddd");
			return false; 
		});

        
	});


	function loadMorePost() {
    if($('body').hasClass('infinite-loading-pending')) return;
    if(!$('.load_more_params').length) return;
    $('body').addClass('infinite-loading-pending');
    
		$.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: 'json',
            data: {
              action: 'load_more_content',
              next_params: $('.load_more_params').data("params"),
            },
            beforeSend: function(){
              $('body').addClass('infinite-loading-pending');
              $('.ajax_loader').fadeIn();
            },
            success: function(response){
              $('body').removeClass('infinite-loading-pending');
              $('.ajax_loader').hide();
              if(response.success){
                if(response.has_posts){
                  var $new_posts = $(response.new_posts);
                  $('.masonry-layout').append($new_posts).masonry( 'appended', $new_posts).imagesLoaded(function  () {
                    $('.masonry-layout').masonry('layout');
                  });
                  

                  if(response.next_params){
                    $('.load_more_params').data("params", response.next_params);
                  }else{
                    $('.load_more_params').remove();
                    $('.load_more_btn').remove();
                    $('.load_more_params').remove();
                    $('.post_data').after('<div class="no-more-posts-message"><span>' + response.no_more_posts_message + '</span></div>');
                  }

                }else{
                  // no more posts
                  $('.load_more_params').remove();
                  $('.post_data').removeClass('with-infinite-scroll');
                  // $('.post_data').append(response.no_more_posts_message);
                }
              }else{
                // error handling
              }
              // $('.isotope-loading').remove();
            }
          });


	}


	function isScrolledIntoView(elem)
	{
	    if($('body').hasClass('with-infinite-button')){
	      // if button was clicked - no need to check if user scrolled into view or not just return true
	      return true;
	    } else {
	      var docViewTop = $(window).scrollTop();
	      var docViewBottom = docViewTop + $(window).height();

	      var elemTop = $(elem).offset().top;
	      var elemBottom = elemTop + $(elem).height();

	      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	    }
	  }

} )( jQuery );