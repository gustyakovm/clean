(function ($) {
    $(function () {
        $('.show_get_policy_form').on('click', function () {
            var link = $(this);
            show_get_policy_form(link);
            return false;
        });


    });

    function show_get_policy_form(link) {
        var provider = link.data('provider_id');
        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                action: 'get_policy_form',
                provider: provider,
                pet_type: link.data('pet_type'),
                deductible: link.data('deductible'),
                copay: link.data('copay'),
                plan_type: link.data('plan_type')
            },
            beforeSend: function () {
                link.text("loading...");
                link.attr("disabled", "disabled");
            },
            success: function (response) {
                link.text("Get Policy");
                link.removeAttr("disabled");
                $.fancybox(response);
                step_forms();
                breed_type_full_action();
                validate_send_form();
                send_get_policy_form(provider);
            }

        });

    }

    function breed_type_full_action() {
      $('#breed_type').change(function () {
        var txt = $('#breed_type option:selected').text();
        $('#breed_type_full').val(txt);
      });
    }

    function send_get_policy_form(provider) {
        var is_first = true;
        // if first submit - just save form
        // second- redirect to url. i made it for healthypaws

        $('#get_policy_frm').submit(function (e) {
            if(is_first){
                $.ajax({
                    type: "POST",
                    url: ajaxurl,
                    dataType: "json",
                    data: {
                        action: 'send_policy_form',
                        formData: $(this).serialize()
                    },
                    beforeSend: function () {
                        $("#get_policy_frm__submit").attr("disabled", "disabled");
                    },
                    success: function (response) {
                        $('.js_previous').hide();
                        $("#get_policy_frm__submit").hide();

                        if(provider != "healthypaws"){
                            $('.get_policy_frm__link').attr('href', response['message']);
                        }

                        $('.get_policy_frm__link').show();
                        get_policy_link_action();
                    }

                });
                e.preventDefault();
            }
            is_first = false;
        });
    }


    function get_policy_link_action() {
        $('.get_policy_frm__link').click(function () {
            var link = $(this);
            var sendData = "";
            if(link.is('input')){
                sendData = link.data('href');
            } else {
                sendData = link.attr('href');
            }
            fbq.push(['track', 'plans']);
            goog_report_conversion(sendData, "_blank");
            $.fancybox.close();
        });
    }

    function validate_send_form() {
        $.validate({
            form: '#get_policy_frm',
        });
        // $.fancybox.update()
    }

    function check_first_form() {
        var valid = true;
        var input = $("#pet_name");
        input.removeClass("error");
        $('span.form-error').remove();
        if(input.val() == ""){
            input.addClass("error");
            $('<span class="help-block form-error">This is a required field</span>').insertAfter('#pet_name');
            input.parent('div').addClass('has-error');
            valid = false;
        }

        return valid;
    }


    /**
     * actions for forms
     */
    function step_forms() {
        var current_fs, next_fs, previous_fs;
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".js_next").click(function () {
             if(!check_first_form())
                 return false;
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on step_progressbar using the index of next_fs
            $("#step_progressbar li").removeClass('active');
            $("#step_progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            next_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".js_previous").click(function () {
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            $("#step_progressbar li").removeClass('active');
            $("#step_progressbar li").eq($("fieldset").index(current_fs) - 1).addClass("active");

            previous_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    scale = 0.8 + (1 - now) * 0.2;
                    left = ((1 - now) * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                easing: 'easeInOutBack'
            });
        });
    }



})(jQuery);