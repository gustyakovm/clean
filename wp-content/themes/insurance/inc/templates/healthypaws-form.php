

<?php $breed_type = get_cookie('breed_type'); ?>
<div class="b-epetsure-form">
    <form id="get_policy_frm" name="get_policy_frm" action="https://www.healthypawspetinsurance.com/Quote" class="form-horizontal" target="_blank" method="post">
        <ul id="step_progressbar">
            <li class="active"><?php echo ucfirst($pet_type); ?> Information</li>
            <li>Pet parent information</li>
        </ul>

        <fieldset>
            <input type="hidden" name="provider_id" value="<?php echo $provider; ?>">
            <input type="hidden" name="PetType" value="<?php echo ucfirst($pet_type); ?>">
            <input type="hidden" name="deductible" value="<?php echo $deductible; ?>">
            <input type="hidden" name="copay" value="<?php echo $copay; ?>">
            <input type="hidden" name="plan_type" value="<?php echo $plan_type; ?>">
            <input type="hidden" id="breed_type_full" name="breed_type_full" value="<?php echo $breed_type ?>">

            <div class="form-group">
                <label class="control-label col-sm-4" for="Name">Pet Name</label>
                <div class="col-sm-8">
                    <input type="text" size="60" data-validation="required" class="form-control" id="pet_name" name="Name" value="<?php echo get_cookie('pet_name'); ?>">
                </div>
            </div>

            <?php
            $dob_month = get_cookie('dob_month');
            $dob_year = get_cookie('dob_year');
            if(!$dob_year) $dob_year = date('Y') - 1;

            ?>
            <div class="form-group">
                <label class="control-label col-sm-4" for="PetBirthMonth">
                    <span class="ep-tooltip" data-tooltip="If you do not know your pet’s exact date of birth, please provide your best estimate.">
                        Date of Birth <i class="fa fa-question-circle"></i>
                    </span>
                </label>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <select name="PetBirthMonth" id="PetBirthMonth" class="form-control">
                                <?php foreach (get_months() as $num => $month) { ?>
                                    <option <?php if($num == $dob_month) echo "selected='selected'" ?> value="<?php echo $num ?>"><?php echo $month ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <select name="PetBirthyear" id="PetBirthyear" class="form-control">
                                <?php foreach (get_years() as $year) { ?>
                                    <option <?php if($year == $dob_year) echo "selected='selected'" ?> value="<?php echo $year ?>"><?php echo $year ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <label class="control-label col-sm-4" for="PetBreed">Breed</label>
                <div class="col-sm-8">
                    <select id="breed_type" name="PetBreed" class="form-control">
                        <?php foreach ($breed_types as $type) { ?>
                            <option <?php if($breed_type == trim($type->name)) echo "selected='selected'" ?> value="<?php echo $type->code; ?>"><?php echo $type->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <?php $sex = get_cookie('sex'); ?>
            <div class="form-group">
                <label class="control-label col-sm-4" for="sex">Sex</label>
                <div class="col-sm-8">
                    <select name="sex" class="form-control">
                        <option <?php if($sex == "male") echo "selected='selected'" ?> value="male">Male</option>
                        <option <?php if($sex == "female") echo "selected='selected'" ?> value="female">Female</option>
                    </select>
                </div>
            </div>

            <input type="button" class="js_next btn tp-btn tp-btn-orange pull-right" value="Next" />
            <div class="clearfix"></div>
        </fieldset>

        <fieldset>

            <div class="form-group">
                <label class="control-label col-sm-4" for="firstname">First Name</label>
                <div class="col-sm-8">
                    <input type="text" size="60" data-validation="required" class="form-control" name="firstname" placeholder="" value="<?php echo get_cookie('firstname'); ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="lastname">Last Name</label>
                <div class="col-sm-8">
                    <input type="text" size="60" data-validation="required" class="form-control" name="lastname" placeholder="" value="<?php echo get_cookie('lastname'); ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="Email">Email</label>
                <div class="col-sm-8">
                    <input type="email" size="60" data-validation="email" class="form-control" name="Email" placeholder="" value="<?php echo get_cookie('email'); ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-4" for="MailingPostalCode">
                    <span class="ep-tooltip" data-tooltip="Providing the ZIP code allows us to give you the most accurate quote due to geographical variations.">
                        Zip <i class="fa fa-question-circle"></i>
                    </span>
                </label>
                <div class="col-sm-8">

                    <input type="text" data-validation="required" class="form-control" name="MailingPostalCode" maxlength="5" placeholder="90210" value="<?php echo get_cookie('zip'); ?>">
                </div>
            </div>

            <input type="submit" name="submit" data-href="https://www.healthypawspetinsurance.com/Quote" class="btn tp-btn tp-btn-orange pull-right get_policy_frm__link" value="Get Policy">
            <input id="get_policy_frm__submit" type="submit" name="submit" class="btn tp-btn tp-btn-orange pull-right" value="Save">
            <input type="button" class="js_previous btn tp-btn tp-btn-orange pull-right" value="Previous" />
            <div class="clearfix"></div>

        </fieldset>

    </form>
</div>