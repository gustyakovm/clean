<?php 

	add_action('wp_ajax_get_policy_form', 'get_policy_form');
	add_action('wp_ajax_nopriv_get_policy_form', 'get_policy_form');

	add_action('wp_ajax_send_policy_form', 'send_policy_form');
	add_action('wp_ajax_nopriv_send_policy_form', 'send_policy_form');
		
	function get_policy_form()
	{
		global $wpdb;
		$data = array();
		$data['provider'] = $_POST['provider'];
		$data['pet_type'] = $_POST['pet_type'];
		$data['deductible'] = $_POST['deductible'];
		$data['copay'] = $_POST['copay'];
		$data['plan_type'] = $_POST['plan_type'];

		$providersWithIP = array('healthypaws', 'petsbest', 'petplan');
		$provider = (in_array($data['provider'], $providersWithIP)) ? $data['provider'] : "all";

		$data['breed_types'] = get_breed_types($provider, $data['pet_type']);
		if($data['provider'] == "healthypaws"){
			$result = my_load_template_part('inc/templates/healthypaws-form', $data);
		} else{
			$result = my_load_template_part('inc/templates/epetsure-form', $data);
		}

		echo $result;
		wp_reset_postdata();
		wp_die();
	}

	function get_breed_types($provider_id, $pet_type){
		global $wpdb;
		$table = $wpdb->prefix . "epetsure_breed_types";
		return $wpdb->get_results( sprintf("SELECT id, name, code FROM %s where type='%s' and provider_id='%s' ORDER BY id", $table, $pet_type , $provider_id));
	}

	function send_policy_form()
	{
		global $wpdb;

		$formData = array();
		parse_str($_POST['formData'], $formData);
		$provider_id = $formData['provider_id'];
		
		
		$path = "/";
		$host = parse_url(get_option('siteurl'), PHP_URL_HOST);
		$expiry = strtotime('+3 month');

		if($provider_id == "healthypaws"){
			#save cookie
			setcookie('pet_name', $formData['Name'], $expiry, $path, $host);
			setcookie('zip', $formData['MailingPostalCode'], $expiry, $path, $host);
			setcookie('email', $formData['Email'], $expiry, $path, $host);
			setcookie('dob_month', $formData['PetBirthMonth'], $expiry, $path, $host);
			setcookie('dob_year', $formData['PetBirthyear'], $expiry, $path, $host);
			setcookie('sex', $formData['sex'], $expiry, $path, $host);
			setcookie('firstname', $formData['firstname'], $expiry, $path, $host);
			setcookie('lastname', $formData['lastname'], $expiry, $path, $host);
			setcookie('breed_type', trim($formData['breed_type_full']), $expiry, $path, $host);

			$formData['pet_name'] = $formData['Name'];
			$formData['pet_type'] = strtolower($formData['PetType']);
			$formData['breed_type'] = $formData['PetBreed'];
			$formData['zip'] = $formData['MailingPostalCode'];
			$formData['email'] = $formData['Email'];
			$formData['dob_month'] = $formData['PetBirthMonth'];
			$formData['dob_year'] = $formData['PetBirthyear'];

		} else {
			setcookie('pet_name', $formData['pet_name'], $expiry, $path, $host);
			setcookie('zip', $formData['zip'], $expiry, $path, $host);
			setcookie('email', $formData['email'], $expiry, $path, $host);
			setcookie('dob_month', $formData['dob_month'], $expiry, $path, $host);
			setcookie('dob_year', $formData['dob_year'], $expiry, $path, $host);
			setcookie('sex', $formData['sex'], $expiry, $path, $host);
			setcookie('firstname', $formData['firstname'], $expiry, $path, $host);
			setcookie('lastname', $formData['lastname'], $expiry, $path, $host);
			setcookie('breed_type', trim($formData['breed_type_full']), $expiry, $path, $host);

			$table = $wpdb->prefix . "epetsure_providers";
			$url = $wpdb->get_var( sprintf("SELECT url FROM %s where name='%s'", $table, $provider_id));
			if(!$url)
				echo json_encode(array('status' => false, 'message' => "Url not found"));
			else{
				echo json_encode(array('status' => true, 'message' => generate_url($url, $provider_id, $formData)));
			}
		}

		save_user($formData);

		wp_reset_postdata();
		wp_die();
	}

/**
 * @param $url
 * @param $formData
 * @return string
 */
function generate_url($url, $provider_id, $formData){
	$pet_type = $formData['pet_type'];
	$dob = generate_dob($formData);
	if($provider_id == "petplan") {
		$pet_type = ucfirst($pet_type);
		$dob = format_month($formData['dob_month']) . "01" . $formData['dob_year'];
	}

	$url = str_replace("{deductible}", $formData['deductible'] , $url);
	$url = str_replace("{copay}", $formData['copay'] , $url);
	$url = str_replace("{plan_type}", $formData['plan_type'] , $url);
	$url = str_replace("{pet_type}", $pet_type , $url);
	$url = str_replace("{pet_name}", $formData['pet_name'], $url);
	$url = str_replace("{dob}", $dob, $url);
	$url = str_replace("{breed_kind}", $formData['breed_kind'], $url);
	$url = str_replace("{breed_type}", $formData['breed_type'], $url);
	$url = str_replace("{sex}", $formData['sex'], $url);
	$url = str_replace("{email}", $formData['email'], $url);
	$url = str_replace("{firstname}", $formData['firstname'], $url);
	$url = str_replace("{lastname}", $formData['lastname'], $url);
	$url = str_replace("{zip}", $formData['zip'], $url);

	return $url;
}

function generate_dob($formData){
	return $formData['dob_month'] . "/1/"  . $formData['dob_year'];
}

function format_month($month){
	return str_pad($month, 2, 0, STR_PAD_LEFT);
}

/**
 * @param $formData
 */
function save_user($formData)
{
	global $wpdb;
	$table_users = $wpdb->prefix . 'epetsure_users';
	$data = array(
		'provider_id' => $formData['provider_id'],
		'email' => $formData['email'],
		'firstname' => $formData['firstname'],
		'lastname' => $formData['lastname'],
		'zip' => $formData['zip'],
		'pet_name' => $formData['pet_name'],
		'dob' => generate_dob($formData),
		'breed_type' => trim($formData['breed_type_full']),
		'pet_type' => $formData['pet_type'],
		'sex' => $formData['sex'],
		'created_at' => date('Y-m-d H:i:s')
	);
	
	$item = $wpdb->get_row($wpdb->prepare("SELECT email FROM " . $table_users . " WHERE email = %s", $data['email']), ARRAY_A);
	if ($item) {
		$wpdb->update($table_users, $data, array('email' => $item['email']));
	} else {
		$wpdb->insert($table_users, $data);
	}
	echo $wpdb->last_error;
}