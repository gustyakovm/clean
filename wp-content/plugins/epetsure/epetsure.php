<?php
/*
Plugin Name: Epetsure tables
Description: plugin for epetsure.com
Author: Maksim Gustyakov
Version: 1.1
*/

global $custom_table_example_db_version;
global $wpdb;
$custom_table_example_db_version = '1.1'; // version changed from 1.0 to 1.1
define( 'EPETSURE__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define('TABLE_PROVIDERS',$wpdb->prefix . 'epetsure_providers');
define('TABLE_BREED_TYPES',$wpdb->prefix . 'epetsure_breed_types');
define('TABLE_USERS',$wpdb->prefix . 'epetsure_users');

function epetsure_tables_install()
{
    global $wpdb;
    global $custom_table_example_db_version;

    $providers_sql = "CREATE TABLE " . TABLE_PROVIDERS . " (
      id int(11) NOT NULL AUTO_INCREMENT,
      name VARCHAR(200) NOT NULL,
      description VARCHAR(200) NULL,
      url text NULL,
      PRIMARY KEY  (id)
    );";

    $breed_type_sql = "CREATE TABLE " . TABLE_BREED_TYPES . " (
      id int(11) NOT NULL AUTO_INCREMENT,
      provider_id VARCHAR(50)  NULL,
      type ENUM('cat','dog') NOT NULL,
      code VARCHAR(50)  NULL,
      name VARCHAR(200)  NULL,
      PRIMARY KEY  (id)
    );
    ALTER TABLE `wp_epetsure_breed_types` ADD `popular` VARCHAR(5) NOT NULL DEFAULT 'no' AFTER `name`;
    ";

    $users_sql = "CREATE TABLE " . TABLE_USERS ." (
        `id` int(11) NOT NULL,
        `provider_id` varchar(100) NOT NULL,
        `firstname` varchar(100) DEFAULT NULL,
        `lastname` varchar(100) DEFAULT NULL,
        `email` varchar(20) NOT NULL,
        `zip` varchar(11) DEFAULT NULL,
        `pet_name` varchar(150) DEFAULT NULL,
        `dob` varchar(20) DEFAULT NULL,
        `breed_type` varchar(100) NOT NULL,
        `pet_type` varchar(20) DEFAULT NULL,
        `sex` varchar(20) DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ALTER TABLE " . TABLE_USERS . " ADD PRIMARY KEY (`id`);
    ALTER TABLE " . TABLE_USERS . " MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";

    // we do not execute sql directly
    // we are calling dbDelta which cant migrate database
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
//    dbDelta($providers_sql);
//    dbDelta($breed_type_sql);
    //dbDelta($users_sql);

    //custom_table_example_install_data();
}
register_activation_hook(__FILE__, 'epetsure_tables_install');

function getProviders()
{
    global $wpdb;

    return $wpdb->get_results( sprintf("SELECT * FROM %s", TABLE_PROVIDERS));
}

function custom_table_example_install_data()
{
    global $wpdb;
    
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'trupanion', 'description' => 'trupanion'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'healthypaws', 'description' =>  'healthypaws'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'figo', 'description' =>  'figo pet insurance'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'aspca', 'description' =>  'aspca pet health insurance'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'petsbest', 'description' =>  'PetsBest Pet Health Insurance'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'petfirst', 'description' =>  'petfirst'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'petplan', 'description' =>  'Petplan'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'embrace', 'description' =>  'Embrace Pet Insurance'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'nationwide', 'description' =>  'Nationwide Pet Insurance'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => '24petwatch', 'description' =>  '24PetWatch'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'petpremium', 'description' =>  'Pet premium'));
    $wpdb->insert(TABLE_PROVIDERS, array('name' => 'petinsurance', 'description' =>  'Pet Insurance'));
    
}

// register_activation_hook(__FILE__, 'custom_table_example_install_data');


function epetsure_admin_menu()
{
    add_menu_page(__('Epetsure', 'providers_table'), __('Epetsure', 'providers_table'), 'activate_plugins', 'providers', 'epetsure_providers_page_handler');
    add_submenu_page('providers', __('Providers', 'providers_table'), __('Providers', 'providers_table'), 'activate_plugins', 'providers', 'epetsure_providers_page_handler');
    // add new will be described in next part
    add_submenu_page('providers', __('Breed types', 'providers_table'), __('Breed types', 'breed_types_table'), 'activate_plugins', 'breed_types', 'epetsure_breed_types_page_handler');
    add_submenu_page('providers', __('Users', 'users_table'), __('Users', 'users_table'), 'activate_plugins', 'users', 'epetsure_users_page_handler');

    add_submenu_page('providers', __('Add new provider', 'providers_table'), __('Add new provider', 'providers_table'), 'activate_plugins', 'providers_form', 'epetsure_providers_form_page_handler');
    add_submenu_page('providers', __('Add new breed', 'breed_types_table'), __('Add new breed', 'breed_types_table'), 'activate_plugins', 'breed_types_form', 'epetsure_breed_types_form_page_handler');
}

add_action('admin_menu', 'epetsure_admin_menu');


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}


require_once(EPETSURE__PLUGIN_DIR . "providers.php" );
require_once(EPETSURE__PLUGIN_DIR . "breed_types.php" );
require_once(EPETSURE__PLUGIN_DIR . "users.php" );


function epetsure_providers_page_handler()
{
    global $wpdb;

    $table = new Providers();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'providers_table'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Providers', 'providers_table')?> 
            <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=providers_form');?>"><?php _e('Add new', 'providers_table')?></a>
        </h2>
        <?php echo $message; ?>

        <form id="providers-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>
    </div>
<?php
}

function epetsure_breed_types_page_handler()
{
    global $wpdb;

    $table = new Breed_types();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'breed_types_table'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Breed types', 'breed_types_table')?> 
            <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=breed_types_form');?>"><?php _e('Add new', 'breed_types_table')?></a>
        </h2>
        <?php echo $message; ?>

        <form id="breed-types-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>
    </div>
<?php
}


function epetsure_providers_form_page_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'epetsure_providers'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'name' => '',
        'description' => '',
        'url' => ''
    );

    // here we are verifying does this request is post back and have correct nonce
    if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);
        // validate data, and if all ok save item to database
        // if id is zero insert otherwise update
        $item_valid = epetsure_validate_providers($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Item was successfully saved', 'providers_table');
                } else {
                    $notice = __('There was an error while saving item', 'providers_table');
                }
            } else {
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                if ($result) {
                    $message = __('Item was successfully updated', 'providers_table');
                } else {
                    $notice = __('There was an error while updating item', 'providers_table');
                }
            }
        } else {
            // if $item_valid not true it contains error message(s)
            $notice = $item_valid;
        }
    }
    else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Item not found', 'providers_table');
            }
        }
    }

    // here we adding our custom meta box
    add_meta_box('providers_form_meta_box', 'Providers data', 'epetsure_providers_form_meta_box_handler', 'provider', 'normal', 'default');

    ?>
<div class="wrap">
    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Provider', 'providers_table')?> <a class="add-new-h2"
                                href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=providers');?>"><?php _e('Back to list', 'providers_table')?></a>
    </h2>

    <?php if (!empty($notice)): ?>
    <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif;?>
    <?php if (!empty($message)): ?>
    <div id="message" class="updated"><p><?php echo $message ?></p></div>
    <?php endif;?>

    <form id="form" method="POST">
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
        <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
        <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

        <div class="metabox-holder" id="poststuff">
            <div id="post-body">
                <div id="post-body-content">
                    <?php /* And here we call our custom meta box */ ?>
                    <?php do_meta_boxes('provider', 'normal', $item); ?>
                    <input type="submit" value="<?php _e('Save', 'providers_table')?>" id="submit" class="button-primary" name="submit">
                </div>
            </div>
        </div>
    </form>
</div>
<?php
}

function epetsure_breed_types_form_page_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'epetsure_breed_types'; // do not forget about tables prefix

    $message = '';
    $notice = '';

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'code' => 0,
        'type' => '',
        'name' => '',
        'provider_id' => ''
    );

    // here we are verifying does this request is post back and have correct nonce
    if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);
        // validate data, and if all ok save item to database
        // if id is zero insert otherwise update
        $item_valid = epetsure_validate_breed_types($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Item was successfully saved', 'breed_types_table');
                } else {
                    $notice = __('There was an error while saving item', 'breed_types_table');
                }
            } else {
                
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));

                if ($result) {
                    $message = __('Item was successfully updated', 'breed_types_table');
                } else {
                     echo $wpdb->last_query;
                    $notice = __('There was an error while updating item', 'breed_types_table');
                }
            }
        } else {
            // if $item_valid not true it contains error message(s)
            $notice = $item_valid;
        }
    }
    else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Item not found', 'breed_types_table');
            }
        }
    }

    // here we adding our custom meta box
    add_meta_box('breed_types_form_meta_box', 'Breed type data', 'epetsure_breed_types_form_meta_box_handler', 'breed_types', 'normal', 'default');

    ?>
<div class="wrap">
    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Breed type', 'breed_types_table')?> <a class="add-new-h2"
                                href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=breed_types');?>"><?php _e('Back to list', 'breed_types_table')?></a>
    </h2>

    <?php if (!empty($notice)): ?>
    <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif;?>
    <?php if (!empty($message)): ?>
    <div id="message" class="updated"><p><?php echo $message ?></p></div>
    <?php endif;?>

    <form id="form" method="POST">
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
        <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
        <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

        <div class="metabox-holder" id="poststuff">
            <div id="post-body">
                <div id="post-body-content">
                    <?php /* And here we call our custom meta box */ ?>
                    <?php do_meta_boxes('breed_types', 'normal', $item); ?>
                    <input type="submit" value="<?php _e('Save', 'breed_types_table')?>" id="submit" class="button-primary" name="submit">
                </div>
            </div>
        </div>
    </form>
</div>
<?php
}

/**
 * This function renders our custom meta box
 * $item is row
 *
 * @param $item
 */
function epetsure_providers_form_meta_box_handler($item)
{
    ?>

    <h4>Url structure</h4>
   <div>
            &lt;a href="#" class="show_get_policy_form btn tp-btn-orange" data-deductible="100" data-copay="70" data-plan_type="10000" data-provider_id="petplan" data-pet_type="dog"&gt;Get Policy&lt;/a&gt;   
     </div>  
    
    <ul>
        <li>
            <strong style="width: 100px;">Deductible:</strong> {deductible}
        </li>
        <li>
            <strong style="width: 100px;">Copay:</strong> {copay}
        </li>
        <li>
            <strong style="width: 100px;">Plan Type:</strong> {plan_type}
        </li>
        
        <li>
            <strong style="width: 100px;">Pet Type:</strong> {pet_type}
        </li>
        <li>
            <strong style="width: 100px;">Pet name:</strong> {pet_name}
        </li>
        <li>
            <strong style="width: 100px;">Date of Birth:</strong> {dob}
        </li>

        <li>
            <strong style="width: 100px;">Mixed/Unlisted Breed:</strong> {breed_kind}
        </li>
        
        <li>
            <strong style="width: 100px;">Breed Type(Code):</strong> {breed_type}
        </li>
        <li>
            <strong style="width: 100px;">Sex:</strong> {sex}
        </li>
        <li>
            <strong style="width: 100px;">Email:</strong> {email}
        </li>
        <li>
            <strong style="width: 100px;">First name:</strong> {firstname}
        </li>
        <li>
            <strong style="width: 100px;">Last name:</strong> {lastname}
        </li>
        <li>
            <strong style="width: 100px;">Breed Code:</strong> {zip}
        </li>

    </ul>
<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="name"><?php _e('Name', 'providers_table')?></label>
        </th>
        <td>
            <input id="name" name="name" type="text" style="width: 20%" value="<?php echo esc_attr($item['name'])?>"
                   size="50" class="code" placeholder="<?php _e('Providers name', 'providers_table')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="name"><?php _e('description', 'providers_table')?></label>
        </th>
        <td>
            <input id="description" name="description" type="text" style="width: 20%" value="<?php echo esc_attr($item['description'])?>"
                   size="50" class="code" placeholder="<?php _e('Description', 'providers_table')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="url"><?php _e('Url', 'providers_table')?></label>
        </th>
        <td>
            <textarea rows=10 id="url" style="width: 75%" name="url"><?php echo esc_attr($item['url'])?></textarea>
        </td>
    </tr>
    
    </tbody>
</table>
<?php
}

function epetsure_breed_types_form_meta_box_handler($item)
{
    ?>

<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="code"><?php _e('Code', 'breed_types_table')?></label>
        </th>
        <td>
            <input id="code" name="code" type="text" style="width: 70%" value="<?php echo esc_attr($item['code'])?>"
                   size="50" class="code" placeholder="<?php _e('Code', 'breed_types_table')?>" required>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="type"><?php _e('Type', 'breed_types_table')?></label>
        </th>
        <td>
           <select name="type">
               <option <?php if($item['type'] == "cat") echo "selected='selected'" ?> value="cat">Cat</option>
               <option <?php if($item['type'] == "dog") echo "selected='selected'" ?> value="dog">Dog</option>
           </select>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="type"><?php _e('Provider', 'providers_table')?></label>
        </th>
        <td>
           <select name="provider_id">
               <option value="all">All</option>
               <?php $providers = getProviders();
               foreach ($providers as $provider) { ?>
                    <option <?php if($item['provider_id'] == $provider->name) echo "selected='selected'" ?> value="<?php echo $provider->name ?>"><?php echo $provider->name ?></option>    
               <?php } ?>
               
           </select>
        </td>
    </tr>

    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="url"><?php _e('Breed type', 'breed_types_table')?></label>
        </th>
        <td>
            <input id="name" name="name" type="text" style="width: 70%" value="<?php echo esc_attr($item['name'])?>"
                   size="50" class="code" placeholder="<?php _e('Breed type name', 'breed_types_table')?>" required>
        </td>
    </tr>
    
    </tbody>
</table>
<?php
}

/**
 * Simple function that validates data and retrieve bool on success
 * and error message(s) on error
 *
 * @param $item
 * @return bool|string
 */
function epetsure_validate_providers($item)
{
    $messages = array();

    if (empty($item['name'])) $messages[] = __('Name is required', 'providers_table');
    // if (!empty($item['email']) && !is_email($item['email'])) $messages[] = __('E-Mail is in wrong format', 'providers_table');
    // if (!ctype_digit($item['age'])) $messages[] = __('Age in wrong format', 'providers_table');
    //if(!empty($item['age']) && !absint(intval($item['age'])))  $messages[] = __('Age can not be less than zero');
    //if(!empty($item['age']) && !preg_match('/[0-9]+/', $item['age'])) $messages[] = __('Age must be number');
    //...

    if (empty($messages)) return true;
    return implode('<br />', $messages);
}

function epetsure_validate_breed_types($item)
{
    $messages = array();
    if (empty($item['type'])) $messages[] = __('Breed type is required', 'breed_types_table');
    if (empty($item['name'])) $messages[] = __('Name is required', 'breed_types_table');
    if (empty($item['provider_id'])) $messages[] = __('Provider is required', 'providers_table');

    // if (!empty($item['email']) && !is_email($item['email'])) $messages[] = __('E-Mail is in wrong format', 'providers_table');
    // if (!ctype_digit($item['age'])) $messages[] = __('Age in wrong format', 'providers_table');
    //if(!empty($item['age']) && !absint(intval($item['age'])))  $messages[] = __('Age can not be less than zero');
    //if(!empty($item['age']) && !preg_match('/[0-9]+/', $item['age'])) $messages[] = __('Age must be number');
    //...

    if (empty($messages)) return true;
    return implode('<br />', $messages);
}
